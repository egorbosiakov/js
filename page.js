const age = 1980;
const myAge = 1983;
const differenceAge = Math.abs(age - myAge);
if (differenceAge >= 10) {
  console.log("холодно");
} else if (differenceAge >= 6) {
  console.log("теплее");
} else if (differenceAge >= 2) {
  console.log("жарко");
} else {
  console.log("в точку");
}

//////-------------------2

if (age === 1983) {
  console.log("Угадал");
} else if (age > 1983 || age < 1983) {
  console.log("Не угадал");
}
////////----------------3

if (age < 0) {
  console.log(Math.abs(age));
}
//////////////------------4
if (age >= 1838 && age < 1864) {
  console.log("В это время еще был жив Калиновский");
}
///////////////-----------5

const str = `вы ввели – ${age} год`;
console.log(str);

///////////////-------------6

let strName = String(age);
console.log(typeof strName);

////////////////-----------7
if (strName.length <= 3) {
  console.log(strName[2]);
} else {
  console.log(strName[0]);
}
//////////////--------------8

const a = "авптма23вы";
const b = "пт";
if (a.indexOf(b)) {
  console.log("ok");
} else {
  console.log("no");
}
/////////////-----------------9
const d = strName.length;
for (let i = 0; i <= d; i++) {
  console.log(strName[d - i]);
}
///////////////----------------10
for (let i = 0; i < Math.abs(age - myAge) - 1; i++) {
  if (age < myAge) {
    console.log(age + 1);
  } else {
    console.log(age - 1);
  }
}

//////////////--------------- ---11
let q = 0;
for (let i = 0; i < strName.length; i++) {
  if (strName[i] === "0") {
    console.log(q);
  }
}

///////////////------------------ ---13
strName = "22";
strName = strName.replaceAll("2", "3");
console.log(strName);
///////////////-------------------14
